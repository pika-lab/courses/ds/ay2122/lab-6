package it.unibo.ds.ws;

import io.javalin.Javalin;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.OpenApiPlugin;
import io.swagger.v3.oas.models.info.Info;
import it.unibo.ds.ws.users.UserController;
import it.unibo.ds.ws.tokens.TokenController;
import it.unibo.ds.ws.utils.Filters;

public class AuthService {

    private static final String API_VERSION = "0.1.0";
    private static final int DEFAULT_PORT = 10000;

    private final int port;
    private final Javalin server;
    private final Authenticator localAuthenticator = new LocalAuthenticator();

    public AuthService(int port) {
        this.port = port;
        server = Javalin.create(config -> {
            config.enableDevLogging();
            config.registerPlugin(new OpenApiPlugin(getOpenApiOptions()));
        });

        // TODO register localAuthenticator into the server via Filters.

        UserController.of(path("/users")).registerRoutes(server);
        TokenController.of(path("/tokens")).registerRoutes(server);
    }

    private static String path(String subPath) {
        return "/auth/v" + API_VERSION + subPath;
    }

    public static void main(String[] args) {
        new AuthService(args.length > 0 ? Integer.parseInt(args[0]) : DEFAULT_PORT).start();
    }

    public void start() {
        server.start(port);
    }

    public void stop() {
        server.stop();
    }

    private static OpenApiOptions getOpenApiOptions() {
        Info applicationInfo = new Info()
                .title("Auth Service")
                .version(API_VERSION)
                .description("A simple WS managing users and their authorization in totally INSECURE way");
        return new OpenApiOptions(applicationInfo).path("/doc");
    }
}
