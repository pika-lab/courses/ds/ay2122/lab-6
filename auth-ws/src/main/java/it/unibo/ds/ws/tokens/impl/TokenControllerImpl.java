package it.unibo.ds.ws.tokens.impl;

import io.javalin.Javalin;
import io.javalin.http.Context;
import io.javalin.http.HttpResponseException;
import io.javalin.plugin.openapi.dsl.OpenApiBuilder;
import it.unibo.ds.ws.AbstractController;
import it.unibo.ds.ws.Credentials;
import it.unibo.ds.ws.Doc;
import it.unibo.ds.ws.tokens.TokenApi;
import it.unibo.ds.ws.tokens.TokenController;
import it.unibo.ds.ws.utils.Filters;

public class TokenControllerImpl extends AbstractController implements TokenController {
    public TokenControllerImpl(String path) {
        super(path);
    }

    private TokenApi getApi(Context context) {
        return TokenApi.of(getAuthenticatorInstance(context));
    }

    @Override
    public void postToken(Context context) throws HttpResponseException {
        TokenApi api = getApi(context);
        Credentials credentials = deserializeBodyAsSingle(Credentials.class, context);

        context.contentType("application/json").future(
                api.createToken(credentials).thenComposeAsync(this::serializeSingleResult)
        );
    }

    @Override
    public void registerRoutes(Javalin app) {
        // TODO add filter for ensuring that all routes properly handle the Accept header

        app.post(path("/"), this::postToken);
        // or alternatively use the following line to support automatic OpenAPI generation
        // app.post(path("/"), OpenApiBuilder.documented(Doc.Tokens.postToken, this::postToken));
    }
}
